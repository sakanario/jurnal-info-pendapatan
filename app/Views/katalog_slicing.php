<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Jurnal Informasi Pendapatan</title>
    <meta name="description" content="The small framework with powerful features">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="/favicon.ico">
    <!-- <link rel="stylesheet" href="../../public/assets/css-jip/beranda.css"> -->
    <link rel="stylesheet" href="assets/css-jip/katalog.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="css/style.min.css"> -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
</head>

<body>
    <header>
        <!-- <nav class="navbar navbar-light">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <img src="assets/image/ebook-logo.svg" class="img-fluid">
                    </ul>
                    <span class="navbar-text">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #ffffff;">
                                <img src="assets/image/avatar.svg" class="img-fluid">
                                <text>Reza Agika, S.T.</text>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </span>
                </div>
            </div>
        </nav> -->
        <nav class="navbar navbar-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <img src="assets/image/ebook-logo.svg" alt=""class="d-inline-block align-text-top">
                </a>
                <span class="navbar-text">
                    <div class="dropdown">
                        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #ffffff;">
                            <img src="assets/image/avatar.svg" class="img-fluid">
                            <text>Reza Agika, S.T.</text>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </span>
            </div>
            </div>
        </nav>
    </header>

    <div class="container justify-content-center">
        <div class="col">
            <img src="assets/image/header-jurnal.svg" class="card-img-top" alt="...">
        </div>
        <div class="dropdown">
            <p1>Tahun </p1>
            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                2022
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
            </div>
        </div>
        <div class="row justify-content-center" style="top: 30px; position: relative; margin-left: 13px; margin-right: 13px">
            <div class="col-sm-2" style="z-index: 2;">
                <button data-toggle="modal" data-target="#detail-journal-wrapper" style="padding: 0; border: none; background: none">
                    <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                    <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                        <p2 class="text-center">JIP Bulan Januari</p2>
                    </div>
                </button>
            </div>
            <div class="col-sm-2" style="z-index: 2;">
                <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                    <p2 class="text-center">JIP Bulan Februari</p2>
                </div>
            </div>
            <div class="col-sm-2" style="z-index: 2;">
                <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                    <p2 class="text-center">JIP Bulan Maret</p2>
                </div>
            </div>
            <div class="col-sm-2" style="z-index: 2;">
                <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                    <p2 class="text-center">JIP Bulan April</p2>
                </div>
            </div>
            <div class="col-sm-2" style="z-index: 2;">
                <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                    <p2 class="text-center">JIP Bulan Mei</p2>
                </div>
            </div>
            <div class="col-sm-2" style="z-index: 2;">
                <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                    <p2 class="text-center">JIP Bulan Juni</p2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="alas">
                <img src="assets/image/kayu.svg" class="card-img-top" alt="...">
            </div>
        </div>
        <br>
        <div class="row justify-content-center" style="top: 30px; position: relative; z-index: 1; margin-left: 13px; margin-right: 13px">
            <div class="col-sm-2">
                <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                    <p2 class="text-center">JIP Bulan Juli</p2>
                </div>
            </div>
            <div class="col-sm-2">
                <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                    <p2 class="text-center">JIP Bulan Agustus</p2>
                </div>
            </div>
            <div class="col-sm-2">
                <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                    <p2 class="text-center">JIP Bulan September</p2>
                </div>
            </div>
            <div class="col-sm-2">
                <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                    <p2 class="text-center">JIP Bulan Oktober</p2>
                </div>
            </div>
            <div class="col-sm-2">
                <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                    <p2 class="text-center">JIP Bulan November</p2>
                </div>
            </div>
            <div class="col-sm-2">
                <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                <div class="badge bg-dark text-wrap col-sm-12" style="top: -10px">
                    <p2 class="text-center">JIP Bulan Desember</p2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="alas">
                <img src="assets/image/kayu.svg" class="card-img-top" alt="...">
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="detail-journal-wrapper" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content p-3">
                    <div class="row">

                        <div class="col-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row justify-content-center" style="position: relative; z-index: 1; margin-left: 13px; margin-right: 13px">
                                        <img src="assets/image/buku.svg" class="card-img-top" alt="...">
                                    </div>
                                    <div class="row justify-content-center" style="margin-top: -15px;">
                                        <img src="assets/image/kayu-modal.svg" class="card-img-top" alt="...">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-6 flex-column-wrapper">
                            <h4 id="journal_title">JIP Januari 2022</h4>
                            <span id="num_of_pages">42 Halaman</span>

                            <p class="mt-2" id="journal_description">Laporan Jurnal Informasi Pendapatan berisi data sampai dengan bulan Januari
                                2022</p>
                            <button class="btn btn-primary stick-on-bottom" id="btn-lihat">Lihat</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script src=<?= base_url('plugins/jquery/jquery.min.js'); ?>></script>
        <script src=<?= base_url('plugins/jquery-ui/jquery-ui.min.js'); ?>></script>
        <script src=<?= base_url('assets/js/bootstrap.min.js'); ?>></script>
        <script src=<?= base_url("plugins/bootstrap/js/bootstrap.bundle.min.js"); ?>></script>
</body>


</html>