<?= $this->extend('_layouts/_layouts') ?>
<?= $this->section('content') ?>

<section class="content ">
    <div class="container-fluid pt-3">
        <div class="card">
            <div class="card-header d-flex justify-content-end">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create-journal"
                    onclick="handleModalCreate()">
                    + Tambah
                </button>
            </div>

            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Judul</th>
                            <th scope="col">Jumlah Halaman</th>
                            <th scope="col">Tahun</th>
                            <th scope="col">Cover</th>
                            <th scope="col">Terbitkan</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <!-- Show data -->
                        <?php foreach ($journal_list as $index => $item) : ?>

                        <tr>
                            <th scope="row"><?= $index + 1 ?></th>
                            <td> <a href="<?=  base_url($item['pdf_link']) ?>">
                                    <?= $item['journal_title'] ?>
                                </a> </td>
                            <td><?= $item['num_of_pages'] ?></td>
                            <td><?= $item['year'] ?></td>
                            <td>
                                <img class="journal-cover"
                                    src="<?= base_url(getenv('PUBLIC_PATH') .  '/uploads/journal_cover/') . '/' . $item['cover_img'] ?>"
                                    alt="cover">

                            </td>
                            <td>
                                <label class="switch">
                                    <input onchange="handleStatusChange('<?= $item['journal_id'] ?>')" type="checkbox"
                                        <?= ($item['status'] == '1') ? 'checked' : ''; ?>>
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td>
                                <button type="button" class="btn btn-warning"
                                    onclick="handleModalEdit(<?=$item['journal_id']?>)">Edit</button>
                                <button type="button" class="btn btn-danger"
                                    onclick="handleDelete(<?=$item['journal_id']?>)">Hapus</button>
                            </td>
                        </tr>
                        <?php endforeach ?>

                    </tbody>
                </table>

                <!-- No data -->
                <?php if(count($journal_list) <= 0): ?>

                <div class="p-3">
                    <h4 class="text-center">Data tidak ditemukan</h4>
                </div>

                <?php endif ?>



            </div>


        </div>
    </div>



</section>


<?= $this->include('admin/journal/component/add_journal_modal') ?>
<?= $this->include('_component/modal_confirm_delete') ?>


<?= $this->endSection() ?>

<?= $this->section('script') ?>

<?= $this->include('dashboard/script') ?>

<script>
const handleModalCreate = (id) => {
    $('#create-journal-title').html('Tambah Data')
    $('#create-journal-form').attr('action', '<?= base_url('admin/journal/create')?>')

    $($('input[name="journal_title"]')).val('')
    $($('textarea[name="journal_description"]')).val('')
    $($('input[name="num_of_pages"]')).val('')
    $($('input[name="year"]')).val('')
    $($('input[name="pdf_link"]')).val('')

    $('#create-journal').modal('toggle');
}

const handleModalEdit = (id) => {
    $.getJSON('<?= base_url('getJournal/')?>/' + id, (res) => {
        $('#create-journal-title').html('Edit Data')
        $('#create-journal-form').attr('action', '<?= base_url('admin/journal/edit')?>/' + id)

        $($('input[name="journal_title"]')).val(res['journal_title'])
        $($('textarea[name="journal_description"]')).val(res['journal_description'])
        $($('input[name="num_of_pages"]')).val(res['num_of_pages'])
        $($('input[name="year"]')).val(res['year'])
        $($('input[name="pdf_link"]')).val(res['pdf_link'])

        $('#create-journal').modal('toggle');
    });

}
const handleDelete = (id) => {
    $('#confirm-hapus-dialog').modal('toggle');
    $('#delete-button').attr('href', '<?= base_url('admin/journal/delete')?>/' + id)
}

const handleStatusChange = (id) => {
    $.post('<?= base_url('admin/journal/edit-status')?>/' + id, (res) => {
        res = JSON.parse(res)

        if(res.sukses === true){
            // Swal.fire('Berhasil ubah status')
            toastr.success('Berhasil ubah status')
        }
    });
}

const f_hideModal = (id) => {
    $(id).modal('hide')
}
</script>

<?= $this->include('partials/alert') ?>


<?= $this->endSection() ?>