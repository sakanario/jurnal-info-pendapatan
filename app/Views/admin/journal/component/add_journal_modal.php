  <!-- Modal Add-->
  <div class="modal fade" id="create-journal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="create-journal-title">Tambah Data</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="f_hideModal('#create-journal')">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form id="create-journal-form" action="<?= base_url('admin/journal/create') ?>" method="post" enctype="multipart/form-data">
                  <div class="modal-body">
                      <div class="form-group">
                          <label>Judul</label>
                          <input type="text" class="form-control" name="journal_title" placeholder="Masukan Judul">
                      </div>
                      <div class="form-group">
                          <label>Deskripsi</label>
                          <textarea class="form-control" name="journal_description" rows="3"></textarea>
                      </div>
                      <div class="form-group">
                          <label>Jumlah Halaman</label>
                          <input type="number" name="num_of_pages" class="form-control"
                              placeholder="Masukan Jumlah Halaman">
                      </div>
                      <div class="form-group">
                          <label>Tahun</label>
                          <input name="year" type="number" class="form-control" placeholder="Masukan Tahun">
                      </div>
                      <div class="form-group">
                          <label>Link PDF</label>
                          <input name="pdf_link" type="text" class="form-control" placeholder="Masukan Link PDF">
                          <small>
                            Contoh : JIP/januari-2022/index.html
                          </small>
                      </div>
                      <div class="form-group">
                          <label>Cover</label>
                          <input name="cover_img" type="file" class="form-control-file">
                      </div>


                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="f_hideModal('#create-journal')">Tutup</button>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>
              </form>
          </div>
      </div>
  </div>