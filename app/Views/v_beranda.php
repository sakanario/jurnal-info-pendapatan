<?= $this->extend('_layouts/_user_layouts') ?>

<!-- Content -->
<?= $this->section('content') ?>
<div class="container mt-5 justify-content-center">
        <div class="col">
            <!-- Nama akun, need add later -->
            <!-- <p>Wilujeng Sumping Reza Agika, S.T. </p> -->
            <h5> <b>Wilujeng Sumping <?= ucfirst(strtolower($user_data->nama_wilayah))?> 👋</b> </h5>
            <p>e-Book BAPENDA Jawa Barat merupakan sistem informasi yang berfungsi sebagai media kolektifitas buku
                digital milik BAPENDA Jawa Barat yang merangkum mengenai kebijakan dan informasi pendapatan Daerah Jawa
                Barat dalam periode waktu tertentu.</p>
            <h6 class="mt-4 mb-4"><b>Silahkan Pilih Menu :</b> </h6>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-3">
                <div class="card menu">
                    <img src="assets/image/jurnal-informasi-pendapatan.svg" class="card-img-top" alt="...">
                    <div class="card-body" >
                        <h6 class="card-title"> <b>Jurnal Informasi Pendapatan</b> </h6>
                        <p class="card-text">Menu kolektifitas arsip dan Jurnal Informasi Pendapatan BAPENDA Jawa Barat.
                        </p>
                        <div class="text-center menu-button">
                            <a href="<?=base_url('/jip')?>" class="btn" style="color: #1565C0;">Mulai</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="card menu">
                    <img src="assets/image/arsip-lainnya.svg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h6 class="card-title"> <b>Arsip Lainnya</b> </h6>
                        <p class="card-text">Menu kolektifitas arsip data lainnya.</p>
                        <div class="text-center menu-button">
                            <a href="#" class="btn" style="color: #1565C0;">Mulai</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?= $this->endSection() ?>


<!-- Script -->
<?= $this->section('script') ?>


<?= $this->endSection() ?>