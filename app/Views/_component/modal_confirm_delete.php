<div class="modal fade" id="confirm-hapus-dialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Hapus</h5>
        <button onclick="f_hideModal('#confirm-hapus-dialog')" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <h2 class="h2">Anda yakin?</h2>
        <p>Data akan terhapus dari database</p>
      </div>
      <div class="modal-footer">
          <button onclick="f_hideModal('#confirm-hapus-dialog')" type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          <a href="" role="button" id="delete-button" class="btn btn-danger">Hapus</a>
      </div>
    </div>
  </div>
</div>