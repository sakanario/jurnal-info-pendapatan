<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Jurnal Informasi Pendapatan</title>
    <meta name="description" content="The small framework with powerful features">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" type="image/png" href="<?= base_url('/favicon.ico') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css-jip/login.css') ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC')?>" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="container py-5 h-100">
        <div class="row d-flex align-items-center justify-content-center h-100">
            <div class="col-md-8 col-lg-7 col-xl-6">
                <img src="<?= base_url('assets/image/beranda-login.svg') ?>" class="img-fluid">
            </div>
            <div class="col-md-7 col-lg-5 col-xl-5 offset-xl-1">

                <form action="<?= base_url('login') ?>" method="post">
                    <img src="<?= base_url('assets/image/logo-login.svg') ?> " class="img-fluid mx-auto d-block">
                    <h3 class="text-center">Masuk Ke Akun</h3>

                    <?= $this->include('partials/block-alert') ?>

                    <!-- Email input -->
                    <div class="form-outline mb-4">
                        <h5>Username</h5>
                        <label class="form-label" for="form1Example13">Masukan Username Anda</label>
                        <input type="text" name="username" class="form-control form-control-lg"
                            placeholder="Masukan Username" />
                    </div>

                    <!-- Password input -->
                    <div class="form-outline mb-4">
                        <h5>Kata Sandi</h5>
                        <label class="form-label" for="form1Example23">Masukan Kata Sandi yang sesuai</label>
                        <input type="password" name="password" class="form-control form-control-lg"
                            placeholder="Masukan Kata Sandi" />
                    </div>
                    <!-- Submit button -->
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
                </form>
            </div>
        </div>
    </div>

</body>


</html>