<script>
/* Store the element in el */
// let el = document.getElementById('tilt')

let tilts = $('.tilt')

tilts.map((index) => {
    /* Get the height and width of the element */
    const height = tilts[index].clientHeight
    const width = tilts[index].clientWidth

    /*
     * Add a listener for mousemove event
     * Which will trigger function 'handleMove'
     * On mousemove
     */
    tilts[index].addEventListener('mousemove', handleMove)

    /* Add listener for mouseout event, remove the rotation */
    tilts[index].addEventListener('mouseout', function() {
        tilts[index].style.transform = 'perspective(500px) scale(1) rotateX(0) rotateY(0)'
    })

    /* Add listener for mousedown event, to simulate click */
    tilts[index].addEventListener('mousedown', function() {
        tilts[index].style.transform = 'perspective(500px) scale(0.9) rotateX(0) rotateY(0)'
    })

    /* Add listener for mouseup, simulate rtilts[index]ease of mouse click */
    tilts[index].addEventListener('mouseup', function() {
        tilts[index].style.transform = 'perspective(500px) scale(1.1) rotateX(0) rotateY(0)'
    })

    function handleMove(e) {
    /*
     * Get position of mouse cursor
     * With respect to the element
     * On mouseover
     */
    /* Store the x position */
    const xVal = e.layerX
    /* Store the y position */
    const yVal = e.layerY

    /*
     * Calculate rotation valuee along the Y-axis
     * Here the multiplier 20 is to
     * Control the rotation
     * You can change the value and see the results
     */
    const yRotation = 20 * ((xVal - width / 2) / width)

    /* Calculate the rotation along the X-axis */
    const xRotation = -20 * ((yVal - height / 2) / height)

    /* Generate string for CSS transform property */
    const string = 'perspective(500px) scale(1.1) rotateX(' + xRotation + 'deg) rotateY(' + yRotation + 'deg)'

    /* Apply the calculated transformation */
    tilts[index].style.transform = string
}

})
</script>