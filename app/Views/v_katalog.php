<?= $this->extend('_layouts/_user_layouts') ?>

<!-- Content -->
<?= $this->section('content') ?>

<div class="container mt-3 justify-content-center">
    <div class="banner">
        <img src="assets/image/header-jurnal.svg" class="card-img-top" alt="...">
    </div>

    <div class="dropdown mb-3">
        <p1>Tahun </p1>
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <?= $selected_year ?>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

            <?php foreach ($list_of_years as $index => $item) : ?>
            <form action="<?=base_url('/jip')?>" method="get">
                <input name="year" type="text" hidden value="<?= $item ?>">
                <button class="dropdown-item" type="submit"><?= $item ?></button>
            </form>
            <!-- <a class="dropdown-item" href="#"></a> -->
            <?php endforeach?>

        </div>
    </div>
    <div class="row justify-content-center">

        <?php $count = 0;?>
        <?php foreach ($journal_list as $index => $item) : ?>
        <?php $count += 1;?>

        <!-- Journal Item  Loop -->
        <div class="col-sm-3 journal-item-container" style="z-index: 2;">
            <div class="col-10 d-flex justify-content-center m-auto">

                <div id="tilt" class="tilt" onclick="handleJournalClick('<?=$item['journal_id']?>')">
                    <div class="card">
                        <div class="card-body c-card-body">
                            <img src="<?= base_url(getenv('PUBLIC_PATH') .  '/uploads/journal_cover/' . $item['cover_img']) ?>"
                                class="card-img-top" alt="...">
                        </div>
                    </div>
                    <div class="badge bg-dark text-wrap col-sm-12">
                        <p2 class="text-center text-white"><?= $item['journal_title']?></p2>
                    </div>
                </div>
            </div>
        </div>


        <?php if(($index + 1)%4 == 0 ): ?>
        <div class="alas">
            <img src="assets/image/kayu.svg" class="card-img-top" alt="...">
        </div>
        <?php endif ?>

        <?php endforeach ?>

        <?php if(!($count%4 == 0)):?>
        <div class="alas">
            <img src="assets/image/kayu.svg" class="card-img-top" alt="...">
        </div>
        <?php endif?>


    </div>


    <!-- Modal Frame Journal -->
    <div class="modal fade" id="journal-iframe-modal" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="journal-iframe-wrapper modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <iframe id="journal-iframe-link" class="journal-iframe" src="#" frameborder="0" height="100%"
                    width="100%"></iframe>
            </div>
        </div>
    </div>

    <!-- Modal Detail Journal -->
    <div class="modal fade" id="detail-journal-wrapper" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog c-modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content p-3">
                <div class="row">

                    <div class="col-md-5">
                        <div class="card cover-wrapper">
                            <div class="card-body pb-0 cover journal-cover-display">
                                <div class="row justify-content-center"
                                    style="position: relative; z-index: 1; margin-left: 13px; margin-right: 13px">
                                    <img id="cover_img" src="assets/image/buku.svg" class="card-img-top  cover-img"
                                        alt="...">
                                </div>
                                <div class="row justify-content-center" style="margin-top: -15px;">
                                    <img src="<?=base_url('assets/image/kayu-modal.svg')?>  " class="card-img-top"
                                        alt="...">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-7 flex-column-wrapper">
                        <h4 id="journal_title">JIP Januari 2022</h4>
                        <span id="num_of_pages">42 Halaman</span>

                        <div class="row">
                            <div class="col-12 pr-0">
                                <p class="mt-2" id="journal_description">Laporan Jurnal Informasi Pendapatan berisi data
                                    sampai dengan bulan Januari
                                    2022</p>
                            </div>
                            <div class="col-5 d-flex px-0">
                                <img hidden  class="img-fluid m-auto px-0" id="qr_code" src="" alt="">
                            </div>
                        </div>



                        <button class="btn btn-primary stick-on-bottom" id="btn-lihat">Lihat</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>


<!-- Script -->
<?= $this->section('script') ?>


<script>
const handleJournalShow = ($link) => {
    $('#detail-journal-wrapper').modal('toggle');
    $('#journal-iframe-modal').modal('toggle');
    $('#journal-iframe-link').attr('src', $link);
}

const handleJournalClick = (id) => {
    $.getJSON('<?= base_url('getJournal')?>/' + id, (res) => {
        $('#cover_img').attr('src', '<?= base_url(getenv('PUBLIC_PATH') .  '/uploads/journal_cover/')?>/' + res['cover_img'])

        $('#qr_code').attr('src', res['qr'])
        
        $('#journal_title').html(res['journal_title'])
        $('#journal_description').html(res['journal_description'])
        $("#num_of_pages").html(res['num_of_pages'] + ' Halaman')

        $('#btn-lihat').attr('onclick', "handleJournalShow(" + "'" + '<?= base_url()?>/' + res[
            'pdf_link'] + "'" + ")")

        $('#detail-journal-wrapper').modal('toggle');
    });

}
</script>

<?= $this->include('_scripts/script_3d') ?>

<?= $this->endSection() ?>