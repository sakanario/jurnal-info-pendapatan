<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title>e-Book Bapenda Provinsi Jawa Barat</title>
    <link rel="icon" href="<?=base_url('assets/image/ebook.svg')?>">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="<?=base_url('/plugins/fontawesome-free/css/all.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('/ionicons/css/ionicons.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('/plugins/icheck-bootstrap/icheck-bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('/plugins/jqvmap/jqvmap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('/adminLTE/css/adminlte.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('/plugins/daterangepicker/daterangepicker.css')?>">
    <link rel="stylesheet" href="<?=base_url('/plugins/summernote/summernote-bs4.css')?>">
    <link href="<?=base_url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700')?>" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url('assets/css-jip/journal-list.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css-jip/global.css')?>">
    <link rel="stylesheet" href="<?=base_url('toastr/toasrt.css')?>">
    


</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <?= $this->include('_layouts/_partials/navbar') ?>
        <?= $this->include('_layouts/_partials/sidebar') ?>
        <div class="content-wrapper">
            <?= $this->renderSection('content') ?>

        </div>
    </div>

    <?= $this->include('_layouts/_partials/footer') ?>
    <aside class="control-sidebar control-sidebar-dark"></aside>
    </div>
    
    <!-- <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script> -->
    <script src=<?= base_url('plugins/jquery/jquery.min.js'); ?>></script>
    <script src=<?= base_url('plugins/jquery-ui/jquery-ui.min.js'); ?>></script>
    <script src=<?= base_url('assets/js/bootstrap.min.js'); ?>></script>
    <script src=<?= base_url("plugins/bootstrap/js/bootstrap.bundle.min.js"); ?>></script>

    <script src=<?= base_url("plugins/chart.js/Chart.min.js")?>></script>
    <script src=<?= base_url("plugins/sparklines/sparkline.js")?>></script>
    <script src=<?= base_url("plugins/jqvmap/jquery.vmap.min.js")?>></script>
    <script src=<?= base_url("plugins/jqvmap/maps/jquery.vmap.usa.js")?>></script>
    <script src=<?= base_url("plugins/jquery-knob/jquery.knob.min.js")?>></script>
    <script src=<?= base_url("plugins/moment/moment.min.js")?>></script>
    <script src=<?= base_url("plugins/daterangepicker/daterangepicker.js")?>></script>
    <script src=<?= base_url("plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js")?>></script>
    <script src=<?= base_url("plugins/summernote/summernote-bs4.min.js")?>></script>
    <script src=<?= base_url("plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js")?>></script>
    <script src=<?= base_url("adminLTE/js/adminlte.js")?>></script>
    <script src=<?= base_url("plugins/sweetalert2/sweetalert2.all.js")?>></script>
    <script src=<?= base_url("toastr/toastr.js")?>></script>
    
    <?= $this->renderSection('script') ?>
    <script src="/adminLTE/js/demo.js"></script>
</body>

</html>