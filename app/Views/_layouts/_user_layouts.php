<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>e-Book Bapenda Provinsi Jawa Barat</title>
    <link rel="icon" href="<?=base_url('assets/image/ebook.svg')?>">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Boostrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="<?=base_url('/plugins/fontawesome-free/css/all.min.css')?>">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?=base_url('assets/css-jip/journal-list.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css-jip/global.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css-jip/nav.css')?>">
    <link rel="stylesheet" href="assets/css-jip/katalog.css">
    <link rel="stylesheet" href="assets/css-jip/beranda.css">



</head>
<body class="hold-transition sidebar-mini layout-fixed">

    <!-- Navbar  -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-primary">

        <?php if($icon == 'e-book'): ?>
            <img style="height: 10vh;" src="assets/image/ebook-logo.png" class="img-fluid">
        <?php elseif($icon == 'jip'): ?>
            <img style="height: 10vh;" src="<?=base_url('assets/image/jip-logo.svg')?>" class="img-fluid">
        <?php endif ?>


        <button style="background-color: white;border-radius: 11px;" class="navbar-toggler" type="button"
            data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarText">
            <div class="nav-item-wrapper ml-2 my-3">
                <div>
                    <img src="<?=base_url('assets/image/profile.svg')?>" class="profile-img img-fluid">
                    <text class="text-white"><?= $user_data->username ?></text>
                </div>
                <a href="<?=base_url('logout-user')?>"><button class="btn btn-danger">Logout</button></a>  
            </div>
        </div>

        <div class="dropdown user-dropdown">
            <button class="btn dropdown-toggle user-button" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false" style="color: #ffffff;">
                <img src="assets/image/profile.svg" class="profile-img img-fluid">
                <text><?= $user_data->username ?></text>
            </button>
            <div class="dropdown-menu custom-dropdown-menu" aria-labelledby="dropdownMenuButton">

                <a class="dropdown-item text-center" href="<?=base_url('logout-user')?>"> Logout </a>

            </div>
        </div>
    </nav>

    <!-- Content -->
    <div style="margin-top: 110px;">
        <?= $this->renderSection('content') ?>
    </div>


    <!-- Footer -->
    <footer class="d-flex flex-wrap  justify-content-center align-items-center py-3 mt-4 border-top footer-wrapper">
        <div class="col-md-4 d-flex align-items-center justify-content-center ">
            <span class="mb-3 mb-md-0 text-muted">© <?=date("Y")?> e-Book Bapenda Provinsi Jawa Barat</span>
        </div>
    </footer>


    <script src=<?= base_url('plugins/jquery/jquery.min.js'); ?>></script>
    <script src=<?= base_url('plugins/jquery-ui/jquery-ui.min.js'); ?>></script>
    <script src=<?= base_url('assets/js/bootstrap.min.js'); ?>></script>
    <script src=<?= base_url("plugins/bootstrap/js/bootstrap.bundle.min.js"); ?>></script>

    <script src=<?= base_url("plugins/chart.js/Chart.min.js")?>></script>
    <script src=<?= base_url("plugins/sparklines/sparkline.js")?>></script>
    <script src=<?= base_url("plugins/jqvmap/jquery.vmap.min.js")?>></script>
    <script src=<?= base_url("plugins/jqvmap/maps/jquery.vmap.usa.js")?>></script>
    <script src=<?= base_url("plugins/jquery-knob/jquery.knob.min.js")?>></script>
    <script src=<?= base_url("plugins/moment/moment.min.js")?>></script>
    <script src=<?= base_url("plugins/daterangepicker/daterangepicker.js")?>></script>
    <script src=<?= base_url("plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js")?>></script>
    <script src=<?= base_url("plugins/summernote/summernote-bs4.min.js")?>></script>
    <script src=<?= base_url("plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js")?>></script>
    <script src=<?= base_url("adminLTE/js/adminlte.js")?>></script>
    <script src=<?= base_url("plugins/sweetalert2/sweetalert2.all.js")?>></script>

    <?= $this->renderSection('script') ?>
</body>

</html>