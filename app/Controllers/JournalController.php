<?php

namespace App\Controllers;

use App\Models\JournalModel;
use App\Controllers\BaseController;
use Config\Services;
class JournalController extends BaseController
{
    public $model;

    public function __construct()
    {
        $this->model = new JournalModel();
        helper('cookie');
    }

    public function index()
    {
        $data['icon'] = 'jip';
        $token = $this->request->getGet('token');

        if(isset($token)){
            $data['user_data'] = decryptToken($token);
        }else{
            $data['user_data'] = decryptToken($this->request->getCookie('token'));
        }
        
        // initiate parameter
        $parameters = [
            'status' => '1'
        ];

        // fetch data
        $data['journal_list'] = $this->model->where($parameters)->findAll();

        // generate list of years
        $data['list_of_years'] = generateListOfYears($data['journal_list']);

        // use current year or spesific year
        $data['selected_year'] = $this->request->getVar('year') ? $this->request->getVar('year') : date("Y");

        // filter data
        $data['journal_list'] = filterByYear($data['journal_list'],$data['selected_year']);

        // kirim data ke view
        return view('v_katalog', $data);
    }

    public function index_admin()
    {
        // dd(auth()->user()->getGroups());
        // dd(auth()->user()->inGroup('superadmin'));
        
        // fetch data
        $data['journal_list'] = $this->model->findAll();

        // kirim data ke view
        return view('admin/journal/journal_list', $data);
    }

    public function create()
    {
        // lakukan validasi
        $validation = \Config\Services::validation();
        $validation->setRules([
            'journal_title' => 'required',
            'journal_description' => 'required',
            'pdf_link' => 'required',
            'cover_img' => 'uploaded[cover_img]|max_size[cover_img,1024]|ext_in[cover_img,jpg,jpeg,png,gif]'
        ]);
        $isDataValid = $validation->withRequest($this->request)->run();

        // jika data valid, simpan ke database
        if ($isDataValid) {
            $cover_filename = uploadImage($this->request->getFile('cover_img'));

            // insert to db
            $this->model->insert([
                "journal_title" => $this->request->getPost('journal_title'),
                "journal_description" => $this->request->getPost('journal_description'),
                "num_of_pages" => $this->request->getPost('num_of_pages'),
                "year" => $this->request->getPost('year'),
                "cover_img" => $cover_filename,
                "pdf_link" => $this->request->getPost('pdf_link'),
                "status" => "1"
            ]);

            // set success session
            $session = \Config\Services::session();
            $session->setFlashdata('message', 'Berhasil simpan data');

            return redirect('admin/journal');
        } else {
            dd($validation->getErrors());
        }

    }

    public function edit($id)
    {

        // ambil artikel yang akan diedit
        $existingData = $this->model->where('journal_id', $id)->first();

        // lakukan validasi data artikel
        $validation = \Config\Services::validation();
        $validation->setRules([
            'journal_title' => 'required',
            'journal_description' => 'required',
            'pdf_link' => 'required',
        ]);
        $isDataValid = $validation->withRequest($this->request)->run();

        // check apakah di request yg dikirim, ada foto
        if (!empty($_FILES['cover_img']['name'])) {
            // jika ada, maka hapus foto sebelumnya
            removeFile(WRITEPATH . '../public/uploads/journal_cover/' . $existingData['cover_img']);

            // upload foto baru
            $cover_filename = uploadImage($this->request->getFile('cover_img'));
        }

        // jika data valid, maka simpan ke database
        if ($isDataValid) {
            $payload = [
                "journal_title" => $this->request->getPost('journal_title'),
                "journal_description" => $this->request->getPost('journal_description'),
                "num_of_pages" => $this->request->getPost('num_of_pages'),
                "year" => $this->request->getPost('year'),
                "pdf_link" => $this->request->getPost('pdf_link'),
            ];

            if (isset($cover_filename)) {
                $payload['cover_img'] = $cover_filename;
            }

            $this->model->update($id, $payload);

            // set success session
            $session = \Config\Services::session();
            $session->setFlashdata('message', 'Berhasil edit data');

            return redirect('admin/journal');
        }
    }

    public function delete($id)
    {
        // remove cover image
        $existingData = $this->model->where('journal_id', $id)->first();
        removeFile(WRITEPATH . '../public/uploads/journal_cover/' . $existingData['cover_img']);

        // remove data in db
        $this->model->delete(['journal_id', $id]);

        // set success session
        $session = \Config\Services::session();
        $session->setFlashdata('message', 'Berhasil hapus data');

        return redirect('admin/journal');
    }

    public function changeStatus($id)
    {
        try {
            $existingData = $this->model->where('journal_id', $id)->first();

            $newStatus = ($existingData['status'] == '1') ? '0' : '1';

            $this->model->update($id, [
                "status" => $newStatus
            ]);

            $this->model->transComplete();

            return json_encode([
                'sukses' => $this->model->transStatus()
            ]);

        } catch (\Exception $e) {
            return json_encode($e);
        }


    }

    public function getById($id)
    {
        $result = $this->model->where([
            'journal_id' => $id
        ])->first();

        // Add QR Code
        $result['qr'] = generateQR(base_url('jurnal/' . $id . '/?token=' . Services::session()->get('token')));

        return json_encode($result);
    }

    public function showJournal($id){
        $result = $this->model->where([
            'journal_id' => $id
        ])->first();

        $data['url'] = base_url($result['pdf_link']);

        return view('show_journal',$data);
    }
}