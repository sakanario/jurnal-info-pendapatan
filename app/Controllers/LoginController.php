<?php

namespace App\Controllers;

use CodeIgniter\Shield\Controllers\LoginController as ShieldLogin;

class LoginController extends ShieldLogin
{
    protected function getValidationRules(): array
    {
        return setting('Validation.login') ?? [
            'username' => [
                'label' => 'Auth.username',
                'rules' => config('AuthSession')->usernameValidationRules,
            ],
            // 'email' => [
            //     'label' => 'Auth.email',
            //     'rules' => config('AuthSession')->emailValidationRules,
            // ],
            'password' => [
                'label' => 'Auth.password',
                'rules' => 'required',
            ],
        ];
    }
}