<?php
namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
use App\Models\TokenModel;


class EncryptController extends ResourceController
{
    use ResponseTrait;

    public $TokenModel;
     public function __construct()
    {
        $this->TokenModel = new TokenModel();
    }

    // get all product
    public function index()
    {
        $encrypter = Services::encrypter();

        $data = $this->request->getJSON()->data;

        $ciphertext = $encrypter->encrypt(json_encode($data),['key' => getenv('ENCRYPT_KEY')]);

        $token = bin2hex($ciphertext);

        // insert to db
        $expired_date = new \DateTime('tomorrow');
        $expired_date_unix = $expired_date->getTimestamp(); 
        
        $this->TokenModel->insert([
            "token" => $token,
            "expired_at" => $expired_date_unix,
            "is_used" => 0,
        ]);
        
        $response = [
            'status' => 200,
            'error' => null,
            'messages' => [
                'success' => 'Enkripsi Berhasil'
            ],
            'data' => [
                'enkripsi' => $token
            ]
        ];
        return $this->respond($response, 200);
    }


}