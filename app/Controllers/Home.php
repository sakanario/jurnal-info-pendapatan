<?php

namespace App\Controllers;
use Config\Services;
use CodeIgniter\HTTP\RequestInterface;


class Home extends BaseController
{

    public function __construct()
    {
        helper('cookie');
    }

    public function index()
    {
        return view('v_login');
    }

    public function beranda()
    {
        $data['icon'] = 'e-book';
        $token = $this->request->getGet('token');

        if(isset($token)){
            $data['user_data'] = decryptToken($token);
        }else{
            $data['user_data'] = decryptToken($this->request->getCookie('token'));
        }

        // $data['user_data'] = decryptToken("ba7c5a0fa8dbebce230ca0ec3c65e1717101fc1a3b52c7216e0732fb3970a15f69e5b8aa3ce7662debc951722bde1c27d4334db07e055c7176bf24591cd60d04aad6e209e8a8e57ca3a17353a771b22d144ed1a588f81f66e52ca5c0a3652c8926c595ad3cd0ef4b0eb835dddee693c4bc5817c2b0f6f524186de4640a8ec88daf96d95ee7b53f9f47c7f9981dd3fc4fcd75ef77cc6d88331b7e");


        return view('v_beranda',$data);
        
    }

    public function katalog()
    {
        // return view('katalog_slicing');
        return view('test');
    }
    public function dashboard()
    {
        return view('dashboard/dashboard');
    }

    public function logoutUser()
    {
        delete_cookie('token');

        return redirect()->to('http://bestrong.bapenda.jabarprov.go.id/')->withCookies();
    }

    public function error403()
    {
        return view('errors/html/error_403');
    }


}