<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TJournal extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'journal_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'journal_title' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'journal_description' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'num_of_pages' => [
                'type' => 'INT',
                'unsigned' => true,
            ],
            'cover_img' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'year' => [
                'type' => 'INT',
                'unsigned' => true,
            ],
            'pdf_link' => [
                'type' => 'TEXT'
            ],
            'status' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'created_at' => [
                'type' => 'DATE',
                'null' => true,
            ],
            'updated_at' => [
                'type' => 'DATE',
                'null' => true,
            ],
            'deleted_at' => [
                'type' => 'DATE',
                'null' => true,
            ]
        ]);
        $this->forge->addKey('journal_id', true);
        $this->forge->createTable('t_journal');
    }

    public function down()
    {
        $this->forge->dropTable('t_journal');
    }
}