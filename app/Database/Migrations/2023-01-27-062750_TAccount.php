<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TAccount extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'account_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'username' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'unique' => true
            ],
            'password' => [
                'type' => 'TEXT',
            ],
            'sur_name' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'last_login' => [
                'type' => 'DATE',
                'null' => true,
            ],
            'created_at' => [
                'type' => 'DATE',
                'null' => true,
            ],
            'updated_at' => [
                'type' => 'DATE',
                'null' => true,
            ],
            'deleted_at' => [
                'type' => 'DATE',
                'null' => true,
            ]

        ]);
        $this->forge->addKey('account_id', true);
        $this->forge->createTable('t_account');
    }

    public function down()
    {
        $this->forge->dropTable('t_account');
    }
}