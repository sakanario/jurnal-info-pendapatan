<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tokens extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'token_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'token' => [
                'type' => 'TEXT',
            ],
            'expired_at' => [
                'type' => 'TEXT',
            ],
            'is_used' => [
                'type' => 'INT'
            ],
        ]);
        $this->forge->addKey('token_id', true);
        $this->forge->createTable('t_token');
    }

    public function down()
    {
        $this->forge->dropTable('t_token');
    }
}
