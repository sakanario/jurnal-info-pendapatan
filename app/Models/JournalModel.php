<?php

namespace App\Models;

use CodeIgniter\Model;

class JournalModel extends Model
{
    protected $table = 't_journal';
    protected $primaryKey = 'journal_id';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['journal_title', 'journal_description', 'num_of_pages', 'year', 'cover_img', 'pdf_link', 'status'];
}