<?php

namespace App\Models;

use CodeIgniter\Model;

class TokenModel extends Model
{
    protected $table = 't_token';
    protected $primaryKey = 'token_id';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['token','expired_at','is_used'];
}
