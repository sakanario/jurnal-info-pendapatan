<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// Public
$routes->get('/403', 'Home::error403');

// Auth
$routes->get('/login', 'LoginController::loginView');
$routes->post('/login', 'LoginController::loginAction');
$routes->get('/logout-user', 'Home::logoutUser');

service('auth')->routes($routes,  ['except' => ['login']]);

// API
$routes->post('/generate_token', 'EncryptController::index');
$routes->get('getJournal/(:any)', 'JournalController::getById/$1');

// User
$routes->group('',['filter' => 'userfilter'], function ($routes) {
    $routes->get('/', 'Home::beranda');
    $routes->get('/jip', 'JournalController::index');
    $routes->get('/jurnal/(:any)', 'JournalController::showJournal/$1');
});

// Admin
$routes->group('admin',['filter' => 'adminfilter'], function ($routes) {
    $routes->get('/', 'Home::dashboard');
    $routes->get('journal', 'JournalController::index_admin');
    $routes->get('journal/delete/(:any)', 'JournalController::delete/$1');
    $routes->add('journal/create', 'JournalController::create');
    $routes->post('journal/edit/(:any)', 'JournalController::edit/$1');
    $routes->post('journal/edit-status/(:any)', 'JournalController::changeStatus/$1');
});




/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}