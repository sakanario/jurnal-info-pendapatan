<?php

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

function uploadImage($uploadedImg)
{
    $file_ext = pathinfo($uploadedImg->getName(), PATHINFO_EXTENSION);
    $cover_filename = 'cover_' . date("Y-m-d-H:i:s", time()) . '.' . $file_ext;
    $uploadedImg->move(WRITEPATH . '../public/uploads/journal_cover', $cover_filename);

    return $cover_filename;
}

function removeFile($file_path)
{
    if (is_file($file_path)) {
        unlink($file_path);
    }
}

function generateListOfYears($data)
{
    $list_of_year = [];

    foreach ($data as $key => $value) {
        if (!in_array($value['year'], $list_of_year)) {
            array_push($list_of_year, $value['year']);
        }
    }

    return $list_of_year;
}

function filterByYear($data, $yearKeyword)
{
    $result = [];
    foreach ($data as $key => $value) {
        if ($value['year'] == $yearKeyword) {
            array_push($result, $value);
        }
    }

    return $result;
}

function decryptToken($token)
{
    try {
        $encrypter = \Config\Services::encrypter();
    
        $ciphertext = hex2bin($token);
    
        $result = $encrypter->decrypt($ciphertext, ['key' => getenv('ENCRYPT_KEY')]);
    
        return json_decode($result);
        
    } catch (\Throwable $th) {
        return false; 
    }
}

function generateQR($data)
{
    $writer = new PngWriter();

    // Create QR code
    $qrCode = QrCode::create($data)
        ->setEncoding(new Encoding('UTF-8'))
        ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
        ->setSize(300)
        ->setMargin(10)
        ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
        ->setForegroundColor(new Color(0, 0, 0))
        ->setBackgroundColor(new Color(255, 255, 255));

    $result = $writer->write($qrCode);

    
    $dataUri = $result->getDataUri();
    
    return $dataUri;
}