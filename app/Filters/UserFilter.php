<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Config\Services;
use App\Models\TokenModel;


class UserFilter implements FilterInterface
{
    /**
     * Do whatever processing this filter needs to do.
     * By default it should not return anything during
     * normal execution. However, when an abnormal state
     * is found, it should return an instance of
     * CodeIgniter\HTTP\Response. If it does, script
     * execution will end and that Response will be
     * sent back to the client, allowing for error pages,
     * redirects, etc.
     *
     * @param RequestInterface $request
     * @param array|null       $arguments
     *
     * @return mixed
     */

    public $TokenModel;


    public function __construct()
    {
        $this->TokenModel = new TokenModel();
        helper('cookie');
    }

    public function handleTokenSessionExist($request)
    {

        $token = $request->getCookie('token');

        // is Token Cookie Exist?
        if (!isset($token)) {
            // Token Cookie not Exist
            return redirect()->to(base_url('403'));
        } else {
            if (strlen($token) <= 0) {
                // Token Only Empty String
                // Redirect to Forbidden Access 
                return redirect()->to(base_url('403'));
            }
        }

    }

    public function isTokenHasBeenUsedOrExpired($token)
    {
        $token_from_db = $this->TokenModel->where(['token' => $token])->first();

        if (!$token_from_db == null) {
            $isExpired = time() > intval($token_from_db['expired_at']);
            $isHasBeenUsed = $token_from_db['is_used'] == '1';

            // check if has been used or expired
            if ($isHasBeenUsed || $isExpired) {
                return true;
            }
        }

        return false;
    }

    public function markTokenAsUsed($token)
    {
        $token_from_db = $this->TokenModel->where(['token' => $token])->first();

        $this->TokenModel->update($token_from_db['token_id'], [
            'is_used' => '1'
        ]);
    }

    public function saveTokenCookie($token)
    {
        // Get the response object from the service container
        $response = service('response');

        // Set a cookie named "my_cookie" with a value of "123"
        $response->setCookie('token', $token, 3600);

        // Return the response object to save the cookie
        return $response;
    }

    public function before(RequestInterface $request, $arguments = null)
    {
        $token = $request->getGet('token');

        // is Token Exist in URL Params?
        if (isset($token)) {
            // Token Exist in URL Params

            // is Token Valid?
            if (decryptToken($token)) {
                // Valid Token

                // is Has Been Used?
                if ($this->isTokenHasBeenUsedOrExpired($token)) {
                    return $this->handleTokenSessionExist($request);
                }

            } else {
                // InValid Token
                return $this->handleTokenSessionExist($request);
            }
        } else {
            // Token not Exist in URL Params
            return $this->handleTokenSessionExist($request);
        }
    }

    /**
     * Allows After filters to inspect and modify the response
     * object as needed. This method does not allow any way
     * to stop execution of other after filters, short of
     * throwing an Exception or Error.
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array|null        $arguments
     *
     * @return mixed
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {

        $token = $request->getGet('token');

        // is Token Exist in URL Params?
        if (isset($token)) {
            // Token Exist in URL Params

            // is Token Valid?
            if (decryptToken($token)) {
                // Valid Token

                if (!$this->isTokenHasBeenUsedOrExpired($token)) {
                    // mark token as used
                    $this->markTokenAsUsed($token);

                    // store token as cookie
                    return $this->saveTokenCookie($token);

                }

            }
        }
    }
}